package com.quiz.game;

import org.apache.commons.lang3.StringEscapeUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SingleQuizQuestion {
    private String question;
    private String correctAnswer;
    private List<String> badAnswers;

    public SingleQuizQuestion(String question, String correctAnswer, List<String> badAnswers) {
        this.question = question;
        this.correctAnswer = correctAnswer;
        this.badAnswers = badAnswers;
    }

    public List<String> getAllAnswersShuffled(){
        List<String> allAnswers = new ArrayList<String>();
        allAnswers.add(correctAnswer);
        allAnswers.addAll(badAnswers);
        Collections.shuffle(allAnswers);
        return allAnswers;
    }

    public String getQuestion() {
        //return StringEscapeUtils.unescapeHtml4(question);
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getCorrectAnswer() {
        return StringEscapeUtils.unescapeHtml4(correctAnswer);
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public List<String> getBadAnswers() {
        List<String> badAnswersUnescaped = badAnswers.stream()
                .map(s->StringEscapeUtils.unescapeHtml4(s))
                .collect(Collectors.toList());
        return badAnswersUnescaped;
    }

    public void setBadAnswers(List<String> badAnswers) {
        this.badAnswers = badAnswers;
    }
}

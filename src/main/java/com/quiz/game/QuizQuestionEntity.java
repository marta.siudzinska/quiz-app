package com.quiz.game;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(schema = "triviadbquestions", name = "QUIZ_QUESTIONS")
public class QuizQuestionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;
    @Column
    private String question;
    @Column
    private String goodAnswer;
    @ElementCollection
    private List<String> badAnswers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getGoodAnswer() {
        return goodAnswer;
    }

    public void setGoodAnswer(String goodAnswer) {
        this.goodAnswer = goodAnswer;
    }

    public List<String> getBadAnswers() {
        return badAnswers;
    }

    public void setBadAnswers(List<String> badAnswers) {
        this.badAnswers = badAnswers;
    }
}

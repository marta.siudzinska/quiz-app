package com.quiz.game;

import org.apache.commons.lang3.StringEscapeUtils;

import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class QuizGame {

    public static void playQuizGame(List<SingleQuizQuestion> allQuizQuestions) {
        int points = 0;
        //Collections.shuffle(allQuizQuestions);
        for (SingleQuizQuestion quizQuestion : allQuizQuestions) {
            boolean hasUserAnsweredCorrectly = askSingleQuizQuestion(quizQuestion);
            if (hasUserAnsweredCorrectly) {
                points++;
            }
        }

//        for (int i = 0; i < userNumberOfQuestionsChoice; i++) {
//            //int random = new Random().nextInt(allQuizQuestions.size());
//            SingleQuizQuestion quizQuestion = allQuizQuestions.get(i);
//            boolean hasUserAnsweredCorrectly = askSingleQuizQuestion(quizQuestion);
//            if (hasUserAnsweredCorrectly) {
//                points++;
//            }
//        }
        System.out.println("Twoj wynik to: " + points);
    }

    private static boolean askSingleQuizQuestion(SingleQuizQuestion quizQuestion) {
        System.out.println(quizQuestion.getQuestion());
        //System.out.println(quizQuestion.getQuestion());
        System.out.println("poprawna: " + quizQuestion.getCorrectAnswer());
        List<String> allAnswers = quizQuestion.getAllAnswersShuffled();


//        String utf8Str = StringEscapeUtils.unescapeHtml4();
//        System.out.println("petla:");
//        for(int i=0; i<allAnswers.size();i++){
//            int random = new Random().nextInt(allAnswers.size());
//            System.out.println(allAnswers.get(random));
//            allAnswers.remove(random);
//        }
        for (int i = 0; i < allAnswers.size(); i++) {
            System.out.println((i + 1) + ". " + allAnswers.get(i));
        }
        int userAnswer = readNumberFromUser(1, 4) - 1;
        String userAnswerText = allAnswers.get(userAnswer);
        System.out.println("Your choice:" + userAnswerText);
        if (quizQuestion.getCorrectAnswer().equals(allAnswers.get(userAnswer))) {
            System.out.println("Podałeś poprawną odpowiedź!");
            return true;
        } else {
            return false;
        }
    }

    private static int readNumberFromUser(int minimalAllowedNumber, int maxAllowedNumber) {
        int userAnswerIndex;
        Scanner scannerFromKeyboard = new Scanner(System.in);
        do {
            System.out.print("Select answer: ");
            try{
                userAnswerIndex = Integer.parseInt(scannerFromKeyboard.nextLine());
            } catch (NumberFormatException e){
                userAnswerIndex = -1;
            }
            if ((userAnswerIndex < minimalAllowedNumber || userAnswerIndex > maxAllowedNumber)) {
                System.out.println("This is not a valid answer! Pick number from " + minimalAllowedNumber + " to " + maxAllowedNumber + "!");
            }
        }
        while (userAnswerIndex < minimalAllowedNumber || userAnswerIndex > maxAllowedNumber);
        return userAnswerIndex;
    }

}

package com.loaders;

import com.google.gson.Gson;
import com.loaders.dto.TriviaDBQuestionsDTO;
import com.loaders.dto.TriviaDBSingleResultDTO;
import com.quiz.game.SingleQuizQuestion;
import com.sun.javafx.fxml.builder.URLBuilder;
import org.apache.commons.lang3.StringEscapeUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class QuestionsFromTriviaDB implements QuestionsLoader {
    public QuestionsFromTriviaDB() {
    }
//    private URL url;

//    public com.loaders.QuestionsFromTriviaDB(URL url) throws MalformedURLException {
//        this.url = new URL("https://opentdb.com/api.php?amount=20&category=28&type=multiple");
//    }

    public List<SingleQuizQuestion> loadQuestions(int userNumberOfQuestionsChoice) throws IOException {

        //URL website = new URL("https://opentdb.com/api.php?amount=20&category=28&type=multiple");
        //String urlPart1
        //if(userNumberOfQuestionsChoice>50) userNumberOfQuestionsChoice=50;
        URL website = new URL("https://opentdb.com/api.php?amount=" + userNumberOfQuestionsChoice + "&category=28&type=multiple");
        String questionsFromTrivia = downloadTextFromURL(website);
        TriviaDBQuestionsDTO triviaDBQuestionsDTO = convertJsonToTriviaQuestions(questionsFromTrivia);
        return convertToSingleQuizQuestions(triviaDBQuestionsDTO);
    }

    private TriviaDBQuestionsDTO convertJsonToTriviaQuestions(String questionsFromTrivia) {
        Gson gson = new Gson();
        return gson.fromJson(questionsFromTrivia, TriviaDBQuestionsDTO.class);

    }

    private List<SingleQuizQuestion> convertToSingleQuizQuestions(TriviaDBQuestionsDTO triviaDBQuestionsDTO) {
        List<SingleQuizQuestion> allQuizQuestions = new ArrayList<>();
        for (int i = 0; i < triviaDBQuestionsDTO.getResults().size(); i++) {
            TriviaDBSingleResultDTO triviaDBSingleResultDTO = triviaDBQuestionsDTO.getResults().get(i);
            SingleQuizQuestion singleQuizQuestion = new SingleQuizQuestion(triviaDBSingleResultDTO.getQuestion(), triviaDBSingleResultDTO.getCorrect_answer(), triviaDBSingleResultDTO.getIncorrect_answers());
            allQuizQuestions.add(singleQuizQuestion);
        }
        return allQuizQuestions;
    }

    private static String downloadTextFromURL(URL url) throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;

        InputStream in = url.openStream();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            while ((line = reader.readLine()) != null) {
                sb.append(line).append(System.lineSeparator());
            }
        } finally {
            in.close();
        }

        return sb.toString();
    }

}

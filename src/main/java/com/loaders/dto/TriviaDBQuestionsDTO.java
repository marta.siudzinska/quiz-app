package com.loaders.dto;

import java.util.List;

public class TriviaDBQuestionsDTO {
    private int response_code;
    private List<TriviaDBSingleResultDTO> results;

    public List<TriviaDBSingleResultDTO> getResults() {
        return results;
    }

}

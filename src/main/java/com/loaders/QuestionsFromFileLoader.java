package com.loaders;

import com.quiz.game.SingleQuizQuestion;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class QuestionsFromFileLoader implements QuestionsLoader {

    private File fileWithQuestions;

    public QuestionsFromFileLoader() {
        this.fileWithQuestions = new File("C:\\Users\\marta.siudzinska\\Documents\\szkolenia\\JAVA\\Warsztaty\\src\\main\\resources\\quiz.txt");
    }

    public List<SingleQuizQuestion> loadQuestions(int userNumberOfQuestionsChoice) throws FileNotFoundException {
        Scanner in = new Scanner(fileWithQuestions);
        List<SingleQuizQuestion> quizQuestions = new ArrayList<>(3000);
        while (in.hasNextLine()) {
            String question = in.nextLine();
            //System.out.println("question:" + question);

            String correctAnswer = in.nextLine();
            //System.out.println("correct answer:" + correctAnswer);

            List<String> badAnswers = new ArrayList<>(3);
            for (int i = 0; i < 3; i++) {
                badAnswers.add(in.nextLine());
                //System.out.println(badAnswers);
            }
            //System.out.println("bad answers:" + badAnswers);

            SingleQuizQuestion data = new SingleQuizQuestion(question, correctAnswer, badAnswers);
            quizQuestions.add(data);
        }
        Collections.shuffle(quizQuestions);
        return quizQuestions.subList(0,userNumberOfQuestionsChoice);
    }
}

package com.loaders;

import com.quiz.game.SingleQuizQuestion;

import java.io.IOException;
import java.util.List;

public interface QuestionsLoader {
    List<SingleQuizQuestion> loadQuestions(int userNumberOfQuestionsChoice) throws IOException;
}

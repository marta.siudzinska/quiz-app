import com.HibernateUtil;
import com.loaders.QuestionsFromDatabaseLoader;
import com.loaders.QuestionsFromFileLoader;
import com.loaders.QuestionsFromTriviaDB;
import com.loaders.QuestionsLoader;
import com.quiz.game.QuizGame;
import com.quiz.game.QuizQuestionEntity;
import com.quiz.game.SingleQuizQuestion;
import org.hibernate.Session;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class Main {
    public static void main(String[] args) throws IOException {

        fillDatabaseWithQuestions();

        Scanner scannerFileOrTriviaChoice = new Scanner(System.in);

        System.out.print("If you want questons from:");
        System.out.println("file - choose 1");
        System.out.println("internet - choose 2");
        int userChoice = scannerFileOrTriviaChoice.nextInt();
        System.out.print("How many questions do you want?");
        int userNumberOfQuestionsChoice = scannerFileOrTriviaChoice.nextInt();
        QuestionsLoader loader = null;
        switch (userChoice) {
            case 1:
                loader = new QuestionsFromFileLoader();
                break;
            case 2:
                loader = new QuestionsFromTriviaDB();
                break;
            case 3:
                loader = new QuestionsFromDatabaseLoader();
            default:
                System.out.println("niepoprawny wybor!");

        }
        List<SingleQuizQuestion> allQuizQuestions = loader.loadQuestions(userNumberOfQuestionsChoice);
        QuizGame.playQuizGame(allQuizQuestions);


        //String data = Resources.toString(, Charsets.UTF_8);

//        com.loaders.QuestionsFromFileLoader loader = new com.loaders.QuestionsFromFileLoader(new File("C:\\Users\\marta.siudzinska\\Documents\\szkolenia\\JAVA\\Warsztaty\\src\\main\\resources\\quiz.txt"));
//        List<com.quiz.game.SingleQuizQuestion> allQuizQuestions = loader.readAllQuizQuestionsFromFile();
//        com.quiz.game.QuizGame.playQuizGame(allQuizQuestions);


    }

    private static void fillDatabaseWithQuestions() throws IOException {
        Session session = HibernateUtil.getSessionFactory().openSession();

        QuestionsLoader loader = null;
        loader = new QuestionsFromTriviaDB();

        //Set<SingleQuizQuestion> uniqueSetQuizQuestions = new HashSet<>();
        Set<String> uniqueSetQuizQuestions = new HashSet<>();
        for (int i = 0; i < 20; i++) {
            List<SingleQuizQuestion> allQuizQuestions = loader.loadQuestions(10);
            for (SingleQuizQuestion allQuizQuestion : allQuizQuestions) {
                if (uniqueSetQuizQuestions.add(allQuizQuestion.getQuestion())) {
                    session.beginTransaction();
                    QuizQuestionEntity quizQuestionEntity = new QuizQuestionEntity();
                    quizQuestionEntity.setQuestion(allQuizQuestion.getQuestion());
                    quizQuestionEntity.setGoodAnswer(allQuizQuestion.getCorrectAnswer());
                    quizQuestionEntity.setBadAnswers(allQuizQuestion.getBadAnswers());
                    session.persist(quizQuestionEntity);
                    session.getTransaction().commit();
                }
            }
        }

//        List<SingleQuizQuestion> allQuizQuestions = loader.loadQuestions(20);
//        for (SingleQuizQuestion allQuizQuestion : allQuizQuestions) {
//            QuizQuestionEntity quizQuestionEntity = new QuizQuestionEntity();
//            quizQuestionEntity.setQuestion(allQuizQuestion.getQuestion());
//            quizQuestionEntity.setGoodAnswer(allQuizQuestion.getCorrectAnswer());
//            quizQuestionEntity.setBadAnswers(allQuizQuestion.getBadAnswers());
//            session.persist(quizQuestionEntity);
//        }

        session.close();
    }


}

